This repository demonstrates how to use a docker image to automatically test Arduino / STM32 code compilation, the code is compiled with the Arduino software.

[![build status](https://gitlab.com/VictorLamoine/arduino_stm32_ci_test/badges/master/build.svg)](https://gitlab.com/VictorLamoine/arduino_stm32_ci_test/commits/master)

- The CI script is [.gitlab-ci.yaml](.gitlab-ci.yml)
- Use [slepp/arduino](https://hub.docker.com/r/slepp/arduino/) docker image to build for Arduino targets
- Use [victorlamoine/arduino:stm32](https://hub.docker.com/r/victorlamoine/arduino/) docker image to build for STM32 targets

